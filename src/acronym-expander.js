export default function acronymExpander(string, acronymsDictionary) {
  for (let [acronym, explanation] of acronymsDictionary) {
    if (isNotInText(acronym)) continue

    const allOccurrences = new RegExp(acronym, 'g')
    string = string.replace(allOccurrences, `${explanation} (${acronym})`)
  }

  return string

  function isNotInText(acronym) {
    return !~string.indexOf(acronym)
  }
}
