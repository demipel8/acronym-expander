import acronymExpander from '../src/acronym-expander'

test('substitutes all acronyms for its expanded meaning', () => {
  const dictionary = new Map([
    ['LOL', 'Laughing Out Loud'],
    ['ROFL', 'Rolling On Floor Laughing'],
    ['WTF', 'What The Fuck']
  ])

  const originalText = 'WTF? LOL, he fell so hard! ROFL LOL'
  const expandedText = 'What The Fuck (WTF)? Laughing Out Loud (LOL), he fell so hard! Rolling On Floor Laughing (ROFL) Laughing Out Loud (LOL)'

  expect(acronymExpander(originalText, dictionary)).toBe(expandedText)
});
