# Acronym Expander

Requirements: *nodejs*

## Develop

```
npm install
```

## Test

```
npm test
```

## Chrome extension

In order to build the extension run `npm run build`. Then go to 'chrome://extensions/'
in your chrome browser and activate 'Developer mode'. Click in 'Load unpacked extension...' and select folder 'release/chrome/unpacked'

Once it is loaded you can open the extension options and start adding your
acronyms.
