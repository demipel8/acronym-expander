export default function() {
  const ACRONYMS_MAP = 'AcronymsMap'

  function list() {
    return new Promise((resolve, reject) => {
      chrome.storage.sync.get(ACRONYMS_MAP, storage => {
        resolve(storageToDictionary(storage[ACRONYMS_MAP]))
      });
    })
  }

  function save(dictionary) {
    chrome.storage.sync.set({ [ACRONYMS_MAP]: dictionaryToStorage(dictionary) });
  }

  function dictionaryToStorage(dictionary) {
    return JSON.stringify(Array.from(dictionary.entries()))
  }

  function storageToDictionary(storage) {
    return new Map(JSON.parse(storage))
  }

  return {
    list,
    save
  }
}
