import acronymsRepository from './acronyms-repository'

const getElement = document.getElementById.bind(document)
const acronymsList = getElement('acronymsList')
let acronymsDictionaryArray = []

document.addEventListener('DOMContentLoaded', restoreOptions)
getElement('addAcronym').addEventListener('click', () => addAcronymToList('',''))
getElement('save').addEventListener('click', saveOptions)

function restoreOptions() {
  acronymsRepository().list().then(acronymsDictionary => {
    for (let [key, value] of acronymsDictionary) {
      addAcronymToList(key, value)
    }
  })
}

function addAcronymToList(acronym, explanation) {
  const li = document.createElement('li')
  const acronymInput = document.createElement('input')
  const explanationInput = document.createElement('input')

  acronymInput.type = 'text'
  explanationInput.type = 'text'

  acronymInput.value = acronym
  explanationInput.value = explanation

  acronymInput.addEventListener('change', updateAcronym.bind(null, acronymsDictionaryArray.length))
  explanationInput.addEventListener('change', updateAcronymExplanation.bind(null, acronymsDictionaryArray.length))

  li.appendChild(acronymInput)
  li.appendChild(explanationInput)

  acronymsList.appendChild(li)
  acronymsDictionaryArray.push([acronym, explanation])
}

function saveOptions() {
  let acronyms = acronymsDictionaryArray.filter(element => element[0] != '' && element[1] != '')

  acronymsRepository().save(new Map(acronyms))
}

function updateAcronym(index, event) {
  acronymsDictionaryArray[index][0] = event.currentTarget.value
}

function updateAcronymExplanation(index, event) {
  acronymsDictionaryArray[index][1] = event.currentTarget.value
}
