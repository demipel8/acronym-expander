import acronymExpander from '../src/acronym-expander'
import acronymsRepository from './acronyms-repository'

const elements = Array.from(document.getElementsByTagName('*'))
const TEXT_NODE = 3

acronymsRepository().list().then(acronyms => {
  elements.forEach(element => {
    element.childNodes.forEach(node => {
      if (isNotText(node)) return

      const text = node.nodeValue
      const replacedText = acronymExpander(text, acronyms)

      if (textsAreDifferent(text, replacedText)) {
          element.replaceChild(document.createTextNode(replacedText), node);
      }
    })
  })
})

function isNotText(node) {
  return node.nodeType !== TEXT_NODE
}

function textsAreDifferent(originalText, newText) {
  return originalText !== newText
}
